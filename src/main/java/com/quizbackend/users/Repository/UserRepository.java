package com.quizbackend.users.Repository;

import com.quizbackend.users.Document.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, Long> {
    Optional<User> findUserByAccountId(Long accountId);
}
