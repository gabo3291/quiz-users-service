package com.quizbackend.users.Document;

import lombok.Data;
import org.springframework.data.annotation.Id;
import java.util.Date;

@Data
public class User {
    @Id
    private String id;

    private Long accountId;
    private String firstName;
    private String lastName;
    private Date createdDate;
    private Boolean isDeleted;
}
